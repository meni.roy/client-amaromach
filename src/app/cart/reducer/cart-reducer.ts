import {addToCart, removeFromCart, cahckoutSuccess, cahckoutfail, amountChange} from "../actions/cart-actions";

export const cartToken = 'cart';
import {Action, createFeatureSelector, createReducer, createSelector, on} from '@ngrx/store';

export interface CartProduct {
  id: string,
  name: string,
  price: number,
  amount: number,
  amountInStock: number,
  description: string
}

export interface CartState {
  products: CartProduct[],
  error: string
}

export const initialState: CartState = {products: [], error: ""};

const reducer = createReducer(initialState,
  on(addToCart, (state, action) => ({...state, products: [...state.products, {...action.payload, amount: 1}]})),
  on(cahckoutSuccess, (state) => ({...state, products: []})),
  on(cahckoutfail, (state, action) => ({...state, error: action.payload})),
  on(amountChange, (state, action) => ({
    ...state,
    products: state.products.map(product => product.id === action.id ? {...product, amount: action.newAmount} : product)
  })),
  on(removeFromCart, (state, action) => ({
    ...state,
    products: state.products.filter(product => product.id !== action.payload)
  })),
);

export function cartReducer(state: CartState | undefined, action: Action) {
  return reducer(state, action);
}

const selectFeature = createFeatureSelector(cartToken);

export const getCartProducts = createSelector(
  selectFeature, (state: CartState) => state.products
);
export const getCartProductsError = createSelector(
  selectFeature, (state: CartState) => state.error
)
;
