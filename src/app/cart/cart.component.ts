import {Component, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {CartProduct, CartState, getCartProducts, getCartProductsError} from "./reducer/cart-reducer";
import {Observable} from "rxjs";
import {cahckout} from "./actions/cart-actions";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public cartProducts$: Observable<CartProduct[]>;
  public error: Observable<string>;

  constructor(private store: Store<CartState>) {
  }

  ngOnInit() {
    this.cartProducts$ = this.store.pipe(select(getCartProducts));
    this.error = this.store.pipe(select(getCartProductsError));
  }

  trackByFnc(i, item) {
    return item.id;
  }

  checkout() {
    this.store.dispatch(cahckout())
  }
}
