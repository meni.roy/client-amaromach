import {Component, Input, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {CartState} from "../reducer/cart-reducer";
import {amountChange} from "../actions/cart-actions";

@Component({
  selector: 'app-cart-product',
  templateUrl: './cart-product.component.html',
  styleUrls: ['./cart-product.component.css']
})
export class CartProductComponent implements OnInit {
  @Input() cartProduct;

  constructor(private store: Store<CartState>) {
  }

  ngOnInit() {
  }

  onAmountChange(value) {
    this.store.dispatch(amountChange({id: this.cartProduct.id, newAmount: parseInt(value)}))
  }

}
