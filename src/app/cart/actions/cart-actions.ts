import {createAction, props} from '@ngrx/store';
import {Product} from "../../home/reducer/home-reducer";

export const addToCart = createAction('[cart] add', props<{ payload: Product }>());
export const amountChange = createAction('[cart] amount change', props<{ id: string, newAmount: number }>());
export const removeFromCart = createAction('[cart] remove', props<{ payload: string }>());
export const cahckout = createAction('[cart] checkout');
export const cahckoutSuccess = createAction('[cart] checkout success');
export const cahckoutfail = createAction('[cart] checkout error', props<{ payload: string }>());
