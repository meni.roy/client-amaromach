import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HomeModule} from "./home/home.module";
import {CartModule} from "./cart/cart.module";
import {StoreModule} from "@ngrx/store";
import {homeReducer, produtToken} from "./home/reducer/home-reducer";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {cartReducer, cartToken} from "./cart/reducer/cart-reducer";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HomeModule,
    CartModule,
    StoreModule.forRoot({[produtToken]: homeReducer, [cartToken]: cartReducer}),
    StoreDevtoolsModule.instrument({}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
