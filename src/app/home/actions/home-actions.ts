import {createAction, props} from '@ngrx/store';
import {Product} from "../reducer/home-reducer";

export const load = createAction('[products] load');
export const loadSuccess = createAction('[products] load success', props<{ payload: Product[] }>());
export const loadFail = createAction('[products] load fail', props<{ error: string }>());
