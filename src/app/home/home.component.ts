import {Component, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {getProducts, Product, ProductState} from "./reducer/home-reducer";
import {loadSuccess} from "./actions/home-actions";
import {Observable} from "rxjs";
import {getCartProducts} from "../cart/reducer/cart-reducer";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public products$: Observable<Product[]>;

  constructor(private store: Store<{ product: ProductState }>) {
  }

  ngOnInit() {
    this.store.dispatch(loadSuccess({
      payload: [{
        id: "1",
        amountInStock: 0,
        name: "fas",
        price: 10,
        description: "FDs"
      }]
    }));
    this.products$ = this.store.pipe(select(getProducts))
  }

  trackByFnc(i, item) {
    return item.id;
  }

}
