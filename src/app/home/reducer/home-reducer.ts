import {load, loadFail, loadSuccess} from "../actions/home-actions";

export const produtToken = 'product';
import {Action, createFeatureSelector, createReducer, createSelector, on} from '@ngrx/store';

export interface Product {
  id: string,
  name: string,
  price: number,
  amountInStock: number,
  description: string
}

export interface ProductState {
  products: Product[],
  error: string
}

export const initialState: ProductState = {products: [], error: ""};

const reducer = createReducer(initialState,
  on(loadSuccess, (state, action) => ({...state, products: action.payload})),
  on(loadFail, (state, action) => ({...state, error: action.error})),
);

export function homeReducer(state: ProductState | undefined, action: Action) {
  return reducer(state, action);
}

const selectFeature = createFeatureSelector(produtToken);

export const getProducts = createSelector(
  selectFeature, (state: ProductState) => state.products
);
