import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../reducer/home-reducer";
import {select, Store} from "@ngrx/store";
import {addToCart, removeFromCart} from "../../cart/actions/cart-actions";
import {CartState, getCartProducts} from "../../cart/reducer/cart-reducer";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Input() product: Product;
  public isInCart: Observable<boolean>;

  constructor(private store: Store<CartState>) {
  }

  ngOnInit() {
    this.isInCart = this.store.pipe(
      select(getCartProducts),
      map(cartProducts => !!cartProducts.find(cartProduct => cartProduct.id === this.product.id)),
    );

  }

  addToCart() {
    this.store.dispatch(addToCart({payload: this.product}))
  }

  removeFromCart() {
    this.store.dispatch(removeFromCart({payload: this.product.id}))
  }
}
